PI9NOZ Mattermost Static HTML
=============================

Deze repository bevat de statische HTML voor de [mattermost chat server](mattermost.pi9noz.ampr.org)
van [Stichting Scoop Hobbyfonds](https://www.hobbyscoop.nl/onze-stichting/). 

De pagina's bevatten de volgende informatie voor de gebruiker:

| Bestand      | Doel                                                                          |
|:-------------|:------------------------------------------------------------------------------|
| about.html   | Over het doel en de doelgroep van deze Mattermost server                      |
| terms.html   | Gebruikersvoorwaarden. Moet een link naar de Mattermost voorwaarden bevatten. |
| privacy.html | De privacy policy                                                             |
| help.html    | Hulp, tips en veel gestelde vragen en antwoorden bij deze Mattermost.         |


Installeren van deze pagina's op de mattermost server
-----------------------------------------------------
Log in middels ssh op de mattermost server. Clone deze repository op de juiste plek:

```
cd /var/www/html
sudo git clone https://gitlab.com/realrolfje/mattermost-pi9noz.git
```

Voeg de volgende location toe aan `/etc/nginx/sites-available/mattermost`:

```
location /info/ {
	root /var/www/html/mattermost-pi9noz/;
}
```

Herlaad de nginx configuratie met `sudo service nginx reload`.

Test door met een browser https://mattermost.pi9noz.ampr.org/info/about.html te openen.
Als de pagina niet laadt, check dan de nginx logging: `sudo tail /var/log/nginx/error.log`.

Configureer Mattermost zodat de links in de applicatie naar de nieuwe html wijzen. Log
 in als administrator op Mattermost, ga naar "System Console", en selecteer links
 in het menu onder het kopje "CUSTOMIZATION" de link "Legal en Support". Configureer deze
 als volgt:

 | Setting               | URL                                                    |
 |:----------------------|:-------------------------------------------------------|
 | Terms of Service link | `https://mattermost.pi9noz.ampr.org/info/terms.html`   |
 | About link            | `https://mattermost.pi9noz.ampr.org/info/about.html`   |
 | Privacy Policy link   | `https://mattermost.pi9noz.ampr.org/info/privacy.html` |
 | Help link             | `https://mattermost.pi9noz.ampr.org/info/help.html`    |


Updaten van deze pagina's op de mattermost server
-------------------------------------------------

Ophalen van de laatste versie van de git pagina's gaat eenvoudig:

```
cd /var/www/html/mattermost-pi9noz
sudo git pull
```

Test door met een browser https://mattermost.pi9noz.ampr.org/info/about.html te openen.
